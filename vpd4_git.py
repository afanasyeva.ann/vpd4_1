def check(date):
    num_31 = (1, 3, 5, 7, 8, 10, 12)
    num_30 = (4, 6, 9, 11)

    try:
        date = date.split('/')
        if len(date) == 3:
            num = int(date[0])
            mon = int(date[1])
            year = int(date[2])
            if 0 < num < 32 and 0 < mon < 13 and year >= 1970:
                if mon in num_31 and num <= 31 or mon in num_30 and num <= 30 or mon == 2 \
                        and num <= 29 and year % 4 == 0 or mon == 2 and num <= 28 and year % 4 > 0:
                    return True
                else:
                    return False
        else:
            print('Введено недопустимое значение')
    except ValueError:
        print('Введено недопустимое значение')


def data_type(num, mon, year):
    nums = [0, 'first', 'second', 'third',
            'fourth', 'fifth', 'sixth', 'seventh',
            'eighth', 'ninth', 'tenth', 'eleventh',
            'twelfth', 'thirteenth', 'fourteenth', 'fifteenth',
            'sixteenth', 'seventeenth', 'eighteenth', 'nineteenth']
    mons = [0, 'January', 'February', 'March', 'April', 'May', 'June',
            'July', 'August', 'September', 'October', 'November', 'December']

    data = ''
    if num // 10 < 2:
        data = data + nums[num] + ' of ' + mons[mon] + ' ' + str(year)
    elif num // 10 == 2:
        if num % 10 == 0:
            data = data + 'twentieth of ' + mons[mon] + ' ' + str(year)
        else:
            data = data + 'twenty ' + nums[num % 10] + ' of ' + mons[mon] + ' ' + str(year)
    elif num // 10 == 3:
        if num % 10 == 1:
            data = data + 'thirty first of ' + mons[mon] + ' ' + str(year)
        elif num % 10 == 0:
            data = data + 'thirtieth of ' + mons[mon] + ' ' + str(year)
    return data


def main():
    date = input('Введите дату в формате ДД/ММ/ГГГГ: ')
    while not check(date):
        print('Введено недопустимое значение')
        date = input('Введите дату в формате ДД/ММ/ГГГГ: ')
    date = date.split('/')
    num = int(date[0])
    mon = int(date[1])
    year = int(date[2])
    print(data_type(num, mon, year))


if __name__ == '__main__':
    main()
