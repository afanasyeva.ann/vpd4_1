from vpd4_git import check


def test_chek1():
    date = '2/7/2019'
    assert check(date)

def test_chek2():
    date = '21/13/2005'
    assert not check(date)

def test_chek3():
    date = '29/2/2019'
    assert not check(date)

def test_chek4():
    date = '32/9/2239'
    assert not check(date)

def test_chek5():
    date = '31/6/1989'
    assert not check(date)