from vpd4_git import data_type


def test_data1():
    num = 29
    mon = 3
    year = 2003
    assert data_type(num, mon, year)


def test_data2():
    num = 31
    mon = 12
    year = 1986
    assert data_type(num, mon, year)


def test_data3():
    num = 35
    mon = 10
    year = 1974
    assert not data_type(num, mon, year)


def test_data4():
    num = 32
    mon = 7
    year = 2012
    assert not data_type(num, mon, year)


def test_data5():
    num = 13
    mon = 7
    year = 2001
    assert data_type(num, mon, year)
